#pragma once

#include "DBRecord.h"

#include <vector>
#include <fstream>


#include <openssl/conf.h>
#include <openssl/evp.h> 
#include <openssl/err.h> 
#include <openssl/aes.h>

class DBClass
{
public:
	DBClass();
	~DBClass();

	int load(const char * filename, const char * key);
	void printAll();

private:
	std::vector<DBRecord> db;
};