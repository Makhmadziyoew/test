#include "stdafx.h"

#include <iostream>
#include <fstream>

#include <string>

#include <openssl/conf.h>
#include <openssl/evp.h> 
#include <openssl/err.h> 
#include <openssl/aes.h>

#include "DBClass.h"

using namespace std;

void bruteforce(const char* filename, char pass_to_find[5]) {
	

	int length = 0; // Длина 
	int str_length = 0; // Длина шифра


	unsigned char *crypted = new unsigned char[1000]; // Инициализация  массива
	unsigned char *plaintext = new unsigned char[1000]; // дешифрованная строка
	unsigned char *iv = (unsigned char *)"0123456789012345"; // рандомайзер

	int plaintext_length = strlen((char *)plaintext);

	fstream in_crypted(filename, ios::binary | ios::in);
	if (in_crypted.is_open()) {
		in_crypted.read((char*)crypted, 1000);
		str_length = in_crypted.gcount();
	}
	in_crypted.close();

	EVP_CIPHER_CTX *ctx = EVP_CIPHER_CTX_new(); // Создание структуры с настройками метода

	char* password = new char[256];

	for (int a = 0; a < 10; a++) {
		for (int b = 0; b < 10; b++) {
			for (int c = 0; c < 10; c++) {
				for (int d = 0; d < 10; d++) {

					pass_to_find[0] = a + '0'; pass_to_find[1] = b + '0'; pass_to_find[2] = c + '0'; pass_to_find[3] = d + '0'; pass_to_find[4] = '\0';

					sprintf(password, "0000000000000000000000000000%s", pass_to_find);

					EVP_DecryptInit_ex(ctx,
						EVP_aes_256_cbc(), 
						NULL,
						(unsigned char*)password, iv); // Инициализация методом AES

					EVP_DecryptUpdate(ctx,
						plaintext, &length,
						crypted,
						str_length);

					plaintext_length = length;
					EVP_DecryptFinal_ex(ctx, 
						(unsigned char*)plaintext + length,
						&length); // Финальная обработка

					plaintext_length += length;
					plaintext[plaintext_length] = '\0'; // Обозначаем конец строки

					if (plaintext[0] == '{' && plaintext[1] == '\r') {
						cout << "Ключ: " << password << endl << "Данные: " << plaintext << endl;
						return;
					}
				}
			}
		}
	}

	EVP_CIPHER_CTX_free(ctx); // Освобождение памяти
}


int main()
{
	setlocale(LC_ALL, "Russian");

	char pass[5] = "\0";
	bruteforce("17_encrypted", pass);

	DBClass db1; // создаем бд

	char* password = new char[256]; // ключ
	sprintf(password, "0000000000000000000000000000%s", pass); // форматируем ключ

	db1.load("17_encrypted", password); // загружаем запись в бд

	db1.printAll(); // вывести в консоль
	

	system("pause");
}


